var isAnagram = function(s, t) {
    if (s.length !== t.length) {
        return false;
    }
    
    var freq = new Array(26).fill(0);
    for (var i = 0; i < s.length; i++) {
        freq[s.charCodeAt(i) - 'a'.charCodeAt(0)]++;
        freq[t.charCodeAt(i) - 'a'.charCodeAt(0)]--;
        //console.log(freq);
    }
    
    for (var i = 0; i < freq.length; i++) {
        if (freq[i] !== 0) {
            return false;
        }
    }
   
    
    return true;
};
var isAnagram = function(s, t) {
    const ns = s.split('').sort().toString()
    const nt = t.split('').sort().toString()
    return ns==nt;
    };
    
    
const s="abc";
const t="cba";
console.log(isAnagram(s,t));