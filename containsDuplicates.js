//Approach 1
 
var containsDuplicates = function(nums) {
    nums.sort();
    for(let i=0;i<nums.length;i++){
        if(nums[i]==nums[i+1]){
            return true;
        }
    }
    return false;
};

//Approach 2
var containsDuplicates = function(nums) {
    const set=new Set(nums);
    return set.size!=nums.length;
};
const nums=[1,2,3,4,7,9];
console.log(containsDuplicates(nums));




